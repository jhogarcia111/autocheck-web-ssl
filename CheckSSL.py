import ssl
import socket
from datetime import datetime

def RegistrarLog(Resultado, Fecha):
    file = open("IconoVirtual/Resultados_SSL_Check.txt","a")
    file.write('\n' + str(Fecha)  + '\t'+Resultado)    
    file.close()

def verify_ssl_certificate(hostname):
    context = ssl.create_default_context()
    #Fecha actual
    now = datetime.now()
    MiFecha= now.strftime('%d-%m-%Y %H:%M:%S')
    try:
        with socket.create_connection((hostname, 443)) as sock:
            with context.wrap_socket(sock, server_hostname=hostname) as ssock:
                ssock.do_handshake()
                cert = ssock.getpeercert()
                print("Se ejecuta el código a las: ",MiFecha)
                RegistrarLog("Certificado SSL Instalado",MiFecha)
    except : #SSLCertVerificationError
        print(NameError)
        print("¡SSL desinstalado! Se ejecuta el código a las: ",MiFecha)
        RegistrarLog("SSL desinstalado! Se debe Reinstalar el SSL",MiFecha)
#    else:
#        print("Se ejecuta el código a las: ",MiFecha)